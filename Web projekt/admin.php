<?php
include ("server.php");
include ("template/header.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin login</title>
</head>
<body>
    <div style="text-align: center; margin-top: 10vh; font-size:6vh;">
    <form action="" method="POST">
    <label style="color: #bdc3c7;" for="admin">Username:</label><br>
    <input style="height: 6vh; width: 100%; text-align:center; font-size:4vh" type="text" name="admin">
    <br>
    <label style="color: #bdc3c7;" for="adminpassword">Password:</label><br>
    <input style="height: 6vh; width: 100%;  text-align:center; font-size:4vh" type="password" name="adminpassword">
    <button style="width:100%; height:6vh; font-size:4vh; margin-top: 10vh;"type="submit" name="loginadmin">Login</button>
    </form>
    </div>
</body>
</html>

<?php
include("template/footer.php")
?>