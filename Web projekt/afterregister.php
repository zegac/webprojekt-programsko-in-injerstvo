<?php
include ("server.php");
?>

<?php
include ("template/header.php");
?>

<div id="afterregister">

<?php if(isset($_SESSION['success'])): ?>
    <div class="message">
        <?php 
        echo ($_SESSION['success']);
        unset($_SESSION['success']);
        ?>
    </div>
<?php endif ?>

<?php if(isset($_SESSION['username'])): ?>
<p>Welcome <strong> <?php echo $_SESSION['username'];?></strong></p>
<p>Go to our home page right now and <a href="userindex.php"> click here</a></p>
<?php endif ?>
</div>


<?php
include ("template/footer.php");
?>
