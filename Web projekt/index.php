<?php include('server.php'); ?>


<?php
include ("template/header.php");
?>

  <!-- Navbar section-->

    <div class="hateit"></div>
    <header>
        <div class="logo">STREET STRENGHT</div>
        <nav>
            <ul>
                <li><a href="#" class="active">Home</a></li>
                <li><a href="#section1">About Us</a></li>
                <li><a href="#section2">Programs</a></li>
                <li><a href="#section3">Workouts</a></li>
                <li><a href="#section4">Contact</a></li>
                <li><a href="#section5">Log in</a></li>
                <li><a href="#section5">Register</a></li>
            </ul>
        </nav>
        <div class="menu-toggle"><i class="fa fa-bars"></i></div>
    </header>
    <hr class="hr-zegac" id="section1">

    <!-- About section-->

    <section id="about">
        <div class="containter-fluid">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-center">
                        <img class="logo" src="resource/logo.jpg" alt="">
                    </div>
                </div>
                <br>
                <div class="col-md-7 about-text">
                    <h2 class="about-header">About Us</h2>
                    <p class="about-paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum obcaecati aspernatur repellendus quos! Distinctio sequi suscipit provident ab sit culpa, eum architecto praesentium. Commodi saepe voluptates, molestias voluptatibus sint odit?</p>
                    <p class="about-paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus blanditiis veritatis cupiditate, neque non cum, repellat placeat excepturi necessitatibus saepe numquam eaque nisi vitae aliquam error odit labore obcaecati voluptatem.</p>
                    <p><a href="team.php" target="_blank"><button class="btn about-btn btn-lg">Read More >> </button></a></p>
                    <div class="row about-social">
                        <div class="col-1">
                            <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                        </div>
                        <div class="col-1">
                            <a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                        </div>
                        <div class="col-1">
                            <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div>
                        <div class="col-1">
                            <a href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                        </div>
                        <div class="col-8">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr class="hr-zegac">
    <hr class="hr-zegac2" id="section2">

    <!-- Programs section-->

    <section>
        <h1 class="text-center section-header">Programs</h1>
    <div id="programs" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#programs" data-slide-to="0" class="active"></li>
              <li data-target="#programs" data-slide-to="1"></li>
              <li data-target="#programs" data-slide-to="2"></li>
            </ul>
          
            <!-- The slideshow -->
            <div class="carousel-inner text-center">
              <div class="carousel-item active">
                <img src="resource/beginner.jpg" alt="Beginner Programs">
                <div class="carousel-caption">
                        <h3 class="programs-header">Beginner Programs</h3>
                        <p class="programs-paragraph">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellendus omnis temporibus praesentium quia, maiores est cumque, modi et sint quidem repudiandae distinctio magnam eaque reprehenderit! Perferendis totam culpa quasi consequatur.</p>
                      </div> 
                <div class="overlay">
                <div class="carousel-caption"></div> 
                </div>
              </div>
              <div class="carousel-item">
                <img src="resource/intermediate.jpg" alt="Intermediate Programs">
                <div class="carousel-caption">
                        <h3 class="programs-header">Intermediate Programs</h3>
                        <p class="programs-paragraph">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellendus omnis temporibus praesentium quia, maiores est cumque, modi et sint quidem repudiandae distinctio magnam eaque reprehenderit! Perferendis totam culpa quasi consequatur.</p>
                      </div> 
                <div class="overlay">
                <div class="carousel-caption"></div> 
                </div>
              </div>
              <div class="carousel-item">
                <img src="resource/advanced.jpg" alt="Advanced Programs">
                <div class="carousel-caption">
                        <h3 class="programs-header">Advanced Programs</h3>
                        <p class="programs-paragraph">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellendus omnis temporibus praesentium quia, maiores est cumque, modi et sint quidem repudiandae distinctio magnam eaque reprehenderit! Perferendis totam culpa quasi consequatur.</p>
                      </div>  
                <div class="overlay">
                <div class="carousel-caption"></div>  
                </div>
              </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#programs" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#programs" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
          </div>
          <p class="text-center"><a href="#section5"><button class="btn programs-btn btn-lg"  onclick="programbutton()";>Start Now >></button></a></p>
    </section>
    <hr class="hr-zegac">
    <hr class="hr-zegac2" id="section3">

    <!-- Workouts section-->

    <section>
      <h1 class="text-center section-header">Workouts</h1>
      <div id="workouts" class="carousel slide" data-ride="carousel">
  
              <!-- Indicators -->
              <ul class="carousel-indicators">
                <li data-target="#workouts" data-slide-to="0" class="active"></li>
                <li data-target="#workouts" data-slide-to="1"></li>
                <li data-target="#workouts" data-slide-to="2"></li>
              </ul>
            
              <!-- The slideshow -->
              <div class="carousel-inner text-center">
                <div class="carousel-item active">
                  <img src="resource/easy.jpg" alt="Easy">
                  <div class="overlay2">
                  <div class="carousel-caption"></div> 
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="resource/medium.jpg" alt="Medium">
                  <div class="overlay2">
                  <div class="carousel-caption"></div> 
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="resource/hard.jpg" alt="Hard"> 
                  <div class="overlay2">
                  <div class="carousel-caption"></div>  
                  </div>
                </div>
              </div>
              <!-- Left and right controls -->
              <a class="carousel-control-prev" href="#workouts" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#workouts" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
            <p class="text-center"><a href="workouts.php" target="_blank"><button class="btn workouts-btn btn-lg">Try it now >></button></a></p>
      </section>
      <hr class="hr-zegac">
      <hr class="hr-zegac2" id="section4">

    <!-- Contact us section-->
    <div id="contact-us">
    <h1 class="text-center section-header">Contact us</h1>
    <form id="contactform" name="contactform" action="contact.php" onsubmit="return contact_validation()" method="post">
      <div class="contact-form">
        <div class="tekst">
          <label>Full Name :</label>
          <input type="text" name="fullname" id="name" placeholder="Enter your name">
        </div>
        <div class="tekst">
          <label>Email :</label>
          <input type="email" name="email" id="email" placeholder="Enter your email">
        </div>
        <div class="tekst">
          <label>Phone number :</label>
          <input type="text" name="phone_number" id="phone_number" placeholder="Enter your phone">
        </div>
        <div class="tekst">
          <label>Message :</label>
          <textarea id="message" name="message" placeholder="Enter your message for us"></textarea>
          <input type="submit" name="send_message" value="Send Message" class="btn-send">
        </div>
      </div>
    </form>
    </div>

      <hr class="hr-zegac">
      <hr class="hr-zegac2" id="section5">

  <!--Login and register section-->
  <div class="container-fluid">
  <div class="row justify-content-center">
    <div id="login-form">
      <div class="col">
    <!-- Button to open the modal login form -->
        <h1>Welcome Back my Friends!</h1>
        <p>if you already have account click the login button</p>
        <button onclick="document.getElementById('id01').style.display='block'">Log in</button>

    <!-- The Modal -->
      <div id="id01" class="modal">
      <span onclick="document.getElementById('id01').style.display='none'"
      class="close" title="Close">&times;</span>

    <!-- Modal Content -->
    <form autocomplete="off" class="modal-content animate" id="logform" name="logform" action="index.php" onsubmit="return validate_login()" method="post">
      <div class="imgcontainer">
        <i class="fa fa-user-circle"></i>
      </div>

      <div class="container">
        <label for="uname"><b>Username</b></label>
        <input autocomplete="off" type="text" placeholder="Enter Username" name="log_uname">
        <div id="log_username_error" class="val_error"></div>

        <label for="log-psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="log_psw">
         <label>
         <div id="log_psw_error" class="val_error"></div>
          <input type="checkbox" checked="checked" name="remember"> Remember me
        </label>
        <button type="submit" name="loginbtn">Login</button>

      </div>

      <div class="container">
        <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Cancel</button>
        <span class="psw">Forgot <a href="forgotpsw.php" target="_blank">password?</a></span>
    </div>
  </form>
  </div>
</div>
    </div>
    <div id="register-form">
      <div class="col">
    <!-- Button to open the modal login form -->
        <h1>You dont have account? </h1>
        <p>click the register button and join us today</p>
        <button onclick="document.getElementById('id02').style.display='block'">Register</button>

    <!-- The Modal -->
      <div id="id02" class="modal">
      <span onclick="document.getElementById('id02').style.display='none'"
      class="close" title="Close">&times;</span>

    <!-- Modal Content -->
    <form autocomplete="off" action="index.php" class="modal-content animate" id="regform" name="regform"  onsubmit="return validate_register()" method="post">
        <div class="imgcontainer">
            <i class="fa fa-user-circle"></i>
          </div>
    
      <div class="container">
        <label for="reg_uname"><b>Username</b></label>
        <input autocomplete="off" id="reg_uname" type="text" placeholder="Enter Username" name="reg_uname" value="<?php echo $username; ?>">
        <div id="register_username_error" class="val_error"></div>
        <div id="error_msg"></div>
    
        <label for="reg_email"><b>Email</b></label>
        <input autocomplete="off" id="reg_email" type="text" placeholder="Enter Email" name="reg_email" value="<?php echo $email; ?>">
        <div id="register_email_error" class="val_error"></div>
        <div id="error_msg2"></div>
    
        <label for="reg_psw"><b>Password</b></label>
        <input id="reg_psw" type="password" placeholder="Enter Password" name="reg_psw" >
    
        <label for="reg_psw_repeat"><b>Repeat Password</b></label>
        <input id="reg_psw_repeat" type="password" placeholder="Repeat Password" name="reg_psw_repeat">
        <div id="register_psw_error" class="val_error"></div>

        <button type="submit" class="registerbtn" name="registerbtn" id="registerbtn">Register</button>
        <button type="button" onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Cancel</button>
        <p>By creating an account you agree to our <a href="t&p.php" target="_blank">Terms & Privacy</a>.</p>
      </div>
    </form>
  </div>
</div>
    </div>
   
</div>
</div>
<hr class="hr-zegac">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="skripta3.js"></script>
<?php
include ("template/footer.php");
?>
