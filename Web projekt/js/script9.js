// Script for navbar //
$(document).ready(function(){

    $('.menu-toggle').click(function(){
        $('nav').toggleClass("active");
    });

    $(document).on("click", "nav a", function(){
        $('nav').removeClass("active");
    });
});


 // Script for log in  and register form //

var modal = document.getElementById('id01');
var modal2 = document.getElementById('id02');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal || event.target == modal2) {
    modal.style.display = "none";
    modal2.style.display = "none";
  }
}

// Skripta za smooth skrol na anchor linkove

document.querySelectorAll('nav a[href^="#"]').forEach(anchor => {
  anchor.addEventListener('click', function (e) {
      e.preventDefault();

      document.querySelector(this.getAttribute('href')).scrollIntoView({
          behavior: 'smooth'
      });
  });
});

// Skripta za team page

$(document).on("click", "#jedan", function(){
  $('#p1').css({
    'display': 'block',
});
  $('#p2').css({
  'display': 'none',
});
  $('#p3').css({
  'display': 'none',
});
});

$(document).on("click", "#dva", function(){
  $('#p2').css({
    'display': 'block',
});
  $('#p1').css({
  'display': 'none',
});
  $('#p3').css({
  'display': 'none',
});
});

$(document).on("click", "#tri", function(){
  $('#p3').css({
    'display': 'block',
});
  $('#p1').css({
  'display': 'none',
});
  $('#p2').css({
  'display': 'none',
});
});

// Validacija contact us
{
  var fullname = document.forms["contactform"]["name"];
  var email = document.forms["contactform"]["email"];
  var phone = document.forms["contactform"]["phone_number"];
  var message = document.forms["contactform"]["message"];

  fullname.addEventListener("blur", nameVerify, true);
  email.addEventListener("blur", emailVerify, true);
  phone.addEventListener("blur", phoneVerify, true);
  message.addEventListener("blur", messageVerify, true);

  function contact_validation()
  {
      // fullname empty
      if(fullname.value == ""){
          fullname.style.background = "#e74c3c";
          fullname.style.color = "#ecf0f1";
          fullname.classList.add("zegac77");
          fullname.focus();
          return false;
      }
      // email empty
      if(email.value == ""){
        email.style.background = "#e74c3c";
        email.style.color = "#ecf0f1";
        email.classList.add("zegac77");
        email.focus();
        return false;
      }
      // email validation
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var state = re.test(String(email.value).toLowerCase());
      if(!state){
        email.style.background = "#e74c3c";
        email.style.color = "#ecf0f1";
        email.classList.add("zegac77");
        email.value = "";
        email.placeholder ="Wrong email format"
        email.focus();
        return false;
      }
      // phone number empty
      if(phone.value == ""){
        phone.style.background = "#e74c3c";
        phone.style.color = "#ecf0f1";
        phone.classList.add("zegac77");
        phone.focus();
        return false;
    }
    //phone number validation
    var phoneNum = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/; 
        if(!phone.value.match(phoneNum)) {
          phone.style.background = "#e74c3c";
          phone.style.color = "#ecf0f1";
          phone.classList.add("zegac77");
          phone.value = "";
          phone.placeholder ="Wrong format"
          phone.focus();
          return false;
        }
    //message empty
        if(message.value == ""){
          message.style.background = "#e74c3c";
          message.style.color = "#ecf0f1";
          message.classList.add("zegac77");
          message.focus();
          return false;
        }
    //message more the 100 charachters
    if(message.value.length < 100){
      message.style.background = "#e74c3c";
      message.style.color = "#ecf0f1";
      message.classList.add("zegac77");
      message.value = "";
      message.placeholder ="Must be more then 100 characters"
      message.focus();
      return false;
    }

  }
  function nameVerify()
  {
      if(fullname.value != "") {
          fullname.style.background = "#2ecc71";
          fullname.style.color = "#ecf0f1";
          return true;
      }
  }
  function emailVerify()
  {
      if(email.value != "") {
        email.style.background = "#2ecc71";
        email.style.color = "#ecf0f1";
        return true;
      }
  }
  function phoneVerify()
  {
      if(phone.value != "") {
        phone.style.background = "#2ecc71";
        phone.style.color = "#ecf0f1";
        return true;
      }
  }

  function messageVerify()
  {
      if(message.value != "") {
        message.style.background = "#2ecc71";
        message.style.color = "#ecf0f1";
        return true;
      }
  }


}
// Validacija login
{
  var username_login = document.forms["logform"]["log_uname"];
  var password_login = document.forms["logform"]["log_psw"];
  
  var username_error_login = document.getElementById("log_username_error");
  var password_error_login = document.getElementById("log_psw_error");
  
  username_login.addEventListener("blur", usernameVerifyLogin, true);
  password_login.addEventListener("blur", passwordVerifyLogin, true);
  
  function validate_login()
  {
      // username empty
      if(username_login.value == ""){
          username_login.style.border = "1px solid #c0392b";
          username_error_login.textContent= "Please enter your username";
          username_login.focus();
          return false;
      }
      // password empty
      if(password_login.value == ""){
          password_login.style.border = "1px solid #c0392b";
          password_error_login.textContent= "Please enter your password";
          password_login.focus();
          return false;
      }
  }
  
  function usernameVerifyLogin()
  {
      if(username_login.value != "") {
          username_login.style.border = "1px solid #27ae60";
          username_error_login.innerHTML = "";
          return true;
      }
  }
  function passwordVerifyLogin()
  {
      if(password_login.value != "") {
          password_login.style.border = "1px solid #27ae60";
          password_error_login.innerHTML = "";
          return true;
      }
  }
  }
  
  // Validacija register
  
  {
  var username_register = document.forms["regform"]["reg_uname"];
  var password_register = document.forms["regform"]["reg_psw"];
  var password2_register = document.forms["regform"]["reg_psw_repeat"];
  var email_register = document.forms["regform"]["reg_email"];
  
  var username_error_register = document.getElementById("register_username_error");
  var password_error_register = document.getElementById("register_psw_error");
  var email_error_register = document.getElementById("register_email_error");
  
  username_register.addEventListener("blur", usernameVerifyRegister, true);
  password_register.addEventListener("blur", passwordVerifyRegister, true);
  password2_register.addEventListener("blur", password2VerifyRegister, true);
  email_register.addEventListener("blur", emailVerifyRegister, true);
  
  function validate_register()
  {
      // username empty
      if(username_register.value == ""){
          username_register.style.border = "1px solid #c0392b";
          username_error_register.textContent= "Username is required";
          username_register.focus();
          return false;
      }
      // username between 6 and 12 characters
      if(username_register.value.length<6 ||username_register.value.length>12){
          username_register.style.border = "1px solid #c0392b"
          username_error_register.textContent= "Username must be between 6 and 12"
          username_register.focus();
          return false;
      }
      //email empty
      if(email_register.value == ""){
          email_register.style.border = "1px solid #c0392b";
          email_error_register.textContent= "Email is required";
          email_register.focus();
          return false;
      }
      //email validation
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var state = re.test(String(email_register.value).toLowerCase());
      if(state == true){}
      else {
          email_register.style.border = "1px solid #c0392b";
          email_error_register.innerHTML= "Email is not good <br> Example of good e-mail : name.lastname@gmail.com";
          email_register.focus();
          return false;
      }
      // password empty
      if(password_register.value == ""){
          password_register.style.border = "1px solid #c0392b";
          password2_register.style.border = "1px solid #c0392b";
          password_error_register.textContent= "Password is required";
          password_register.focus();
          return false;
      }
      // password between 6 and 12 characters
      if(password_register.value.length<6 ||password_register.value.length>12){
          password_register.style.border = "1px solid #c0392b"
          password2_register.style.border = "1px solid #c0392b";
          password_error_register.textContent= "Password must be between 6 and 12"
          password_register.focus();
          return false;
      }
      //check if two password are the same
      if(password_register.value != password2_register.value){
          password_register.style.border = "1px solid #c0392b";
          password2_register.style.border = "1px solid #c0392b";
          password_error_register.textContent= "The two passwords do not match"
          return false;
      }
  }
  function usernameVerifyRegister()
  {
      if(username_register.value != "") {
          username_register.style.border = "1px solid #27ae60";
          username_error_register.innerHTML = "";
          return true;
      }
  }
  function passwordVerifyRegister()
  {
      if(password_register.value != "") {
          password_register.style.border = "1px solid #27ae60";
          password_error_register.innerHTML = "";
          return true;
      }
  }
  
  function password2VerifyRegister()
  {
      if(password2_register.value != "") {
          password2_register.style.border = "1px solid #27ae60";
          password2_error_register.innerHTML = "";
          return true;
      }
  }
  
  function emailVerifyRegister()
  {
      if(email_register.value != "") {
          email_register.style.border = "1px solid #27ae60";
          email_error_register.innerHTML = "";
          return true;
      }
  }
  }
  
// Programs start now button
function programbutton(){
var a = confirm("If you wanna see this section you need first log in!");
if (a==true){
document.getElementById("id01").style.display="block";
}
else{
  return false;
}
}
