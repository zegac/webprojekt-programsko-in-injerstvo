<?php
include ("server.php");
if(empty($_SESSION['admin'])){
  header('location: index.php');
}
?>

<?php
include ("template/header.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Uplaud content</title>

</head>
<body>
<div id="content_upload">
  <form action="" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="size" value="1000000">
      <div>
          <input type="file" name="image">
      </div>
      <div>
          <textarea name="text" placeholder="Say something about this image"></textarea>
      </div>
      <div>
          <input type="submit" name="upload" value="Upload image">
      </div>
  </form>
</div>
<a class="logout-admin" href="index.php?logout='1'">Logout</a>
</body>
</html>

<?php
include("template/footer.php")
?>
