<?php
include ("template/header.php");
?>
<div id="tap">
<h1>Terms & Privacy Policy</h1>

<h5>INTRODUCTION</h5>
<p>This Privacy Policy will inform you about the types of personal data we collect, the purposes for which we use the data, the ways in which the data is handled and your rights with regard to your personal data. Furthermore, this Privacy Policy is intended to satisfy the obligation of transparency under the EU General Data Protection Regulation 2016/679 ("GDPR") and the laws implementing GDPR.</p>

<h5>CHILDREN'S PRIVACY</h5>
<p>Lorem Ipsum does not knowingly collect personally identifiable information (PII) from persons under the age of 13. If you are under the age of 13, please do not provide us with information of any kind whatsoever. If you have reason to believe that we may have accidentally received information from a child under the age of 13, please contact us immediately at legal@wasai.co. If we become aware that we have inadvertently received Personal Information from a person under the age of 13, we will delete such information from our records.</p>

<h5>INFORMATION PROVIDED DIRECTLY BY YOU</h5>
<p>We collect information you provide directly to us, such as when you request information, create or modify your personal account, request Services, subscribe to our Services, complete a Lorem Ipsum form, survey, quiz or application, contact customer support, join or enroll for an event or otherwise communicate with us in any manner. This information may include, without limitation: name, date of birth, e-mail address, physical address, business address, phone number, or any other personal information you choose to provide.</p>

<h5>INFORMATION COLLECTED THROUGH YOUR USE OF OUR SERVICES</h5>
<p>The following are situations in which you may provide Your Information to us:</p>
<ol>
    <li>When you fill out forms or fields through our Services;</li>
    <li>When you register for an account with our Service;</li>
    <li>When you create a subscription for our newsletter or Services;</li>
    <li>When you provide responses to a survey;</li>
    <li>When you answer questions on a quiz;</li>
    <li>When you join or enroll in an event through our Services;</li>
    <li>When you order services or products from, or through our Service;</li>
    <li>When you provide information to us through a third-party application, service or Website;</li>
    <li>When you communicate with us or request information about us or our products or Services, whether via email or other means; and</li>
    <li>When you participate in any of our marketing initiatives, including, contests, events, or promotions.</li>
</ol>
<p>We also automatically collect information via the Website through the use of various technologies, including, but not limited to Cookies and Web Beacons (explained below). We may collect your IP address, browsing behavior and device IDs. This information is used by us in order to enable us to better understand how our Services are being used by visitors and allows us to administer and customize the Services to improve your overall experience.</p>

<h5>INFORMATION COLLECTED FROM OTHER SOURCES</h5>
<p>We may also receive information from other sources and combine that with information we collect through our Services. For example if you choose to link, create, or log in to your Lorem Ipsum account with a social media service, e.g. LinkedIn, Facebook or Twitter, or if you engage with a separate App or Website that uses our API, or whose API we use, we may receive information about you or your connections from that Site or App. This includes, without limitation, profile information, profile picture, gender, user name, user ID associated with your social media account, age range, language, country, friends list, and any other information you permit the social network to share with third parties. The data we receive is solely dependent upon your privacy settings with the social network.</p>

<h5>INFORMATION THIRD PARTIES PROVIDE</h5>
<p>We may collect information about you from sources other than you, such as from social media websites (i.e., Facebook, LinkedIn, Twitter or others), blogs, analytics providers, business affiliates and partners and other users. This includes, without limitation, identity data, contact data, marketing and communications data, behavioral data, technical data and content data.</p>

<h5>AGGREGATED DATA</h5>
<p>We may collect, use and share Aggregated Data such as statistical or demographic data for any purpose. Aggregated Data is de-identified or anonymized and does not constitute Personal Data for the purposes of the GDPR as this data does not directly or indirectly reveal your identity. If we ever combine Aggregated Data with your Personal Data so that it can directly or indirectly identify you, we treat the combined data as PII which will only be used in accordance with this Privacy Policy.</p>

</div>
<?php
include ("template/footer.php");
?>
