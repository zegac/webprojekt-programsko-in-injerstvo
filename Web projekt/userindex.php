<?php
include ("server.php");
if(empty($_SESSION['username'])){
  header('location: index.php');
}
?>

<?php
include ("template/header.php");
?>

  <!-- Navbar section-->

    <div class="hateit"></div>
    <header>
        <div class="logo">STREET STRENGHT</div>
        <nav>
            <ul>
                <li><a href="#" class="active">Home</a></li>
                <li><a href="#section1">About Us</a></li>
                <li><a href="#section2">Programs</a></li>
                <li><a href="#section3">Workouts</a></li>
                <li><a href="#section4">Contact</a></li>
                <li><a href="index.php?logout='1'">Logout</a></li>
                <li><a href="#">Hi, <?php echo $_SESSION['username'];?></a></li>
            </ul>
        </nav>
        <div class="menu-toggle"><i class="fa fa-bars"></i></div>
    </header>
    <hr class="hr-zegac" id="section1">

    <!-- About section-->

    <section id="about">
        <div class="containter-fluid">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-center">
                        <img class="logo" src="resource/logo.jpg" alt="">
                    </div>
                </div>
                <br>
                <div class="col-md-7 about-text">
                    <h2 class="about-header">About Us</h2>
                    <p class="about-paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum obcaecati aspernatur repellendus quos! Distinctio sequi suscipit provident ab sit culpa, eum architecto praesentium. Commodi saepe voluptates, molestias voluptatibus sint odit?</p>
                    <p class="about-paragraph">Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus blanditiis veritatis cupiditate, neque non cum, repellat placeat excepturi necessitatibus saepe numquam eaque nisi vitae aliquam error odit labore obcaecati voluptatem.</p>
                    <p><a href="team.php" target="_blank"><button class="btn about-btn btn-lg">Read More >> </button></a></p>
                    <div class="row about-social">
                        <div class="col-1">
                            <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                        </div>
                        <div class="col-1">
                            <a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
                        </div>
                        <div class="col-1">
                            <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div>
                        <div class="col-1">
                            <a href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                        </div>
                        <div class="col-8">
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr class="hr-zegac">
    <hr class="hr-zegac2" id="section2">

    <!-- Programs section-->

    <section>
        <h1 class="text-center section-header">Programs</h1>
    <div id="programs" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
              <li data-target="#programs" data-slide-to="0" class="active"></li>
              <li data-target="#programs" data-slide-to="1"></li>
              <li data-target="#programs" data-slide-to="2"></li>
            </ul>
          
            <!-- The slideshow -->
            <div class="carousel-inner text-center">
              <div class="carousel-item active">
                <img src="resource/beginner.jpg" alt="Beginner Programs">
                <div class="carousel-caption">
                        <h3 class="programs-header">Beginner Programs</h3>
                        <p class="programs-paragraph">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellendus omnis temporibus praesentium quia, maiores est cumque, modi et sint quidem repudiandae distinctio magnam eaque reprehenderit! Perferendis totam culpa quasi consequatur.</p>
                      </div> 
                <div class="overlay">
                <div class="carousel-caption"></div> 
                </div>
              </div>
              <div class="carousel-item">
                <img src="resource/intermediate.jpg" alt="Intermediate Programs">
                <div class="carousel-caption">
                        <h3 class="programs-header">Intermediate Programs</h3>
                        <p class="programs-paragraph">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellendus omnis temporibus praesentium quia, maiores est cumque, modi et sint quidem repudiandae distinctio magnam eaque reprehenderit! Perferendis totam culpa quasi consequatur.</p>
                      </div> 
                <div class="overlay">
                <div class="carousel-caption"></div> 
                </div>
              </div>
              <div class="carousel-item">
                <img src="resource/advanced.jpg" alt="Advanced Programs">
                <div class="carousel-caption">
                        <h3 class="programs-header">Advanced Programs</h3>
                        <p class="programs-paragraph">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellendus omnis temporibus praesentium quia, maiores est cumque, modi et sint quidem repudiandae distinctio magnam eaque reprehenderit! Perferendis totam culpa quasi consequatur.</p>
                      </div>  
                <div class="overlay">
                <div class="carousel-caption"></div>  
                </div>
              </div>
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#programs" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#programs" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
          </div>
          <p class="text-center"><a href="userprograms.php"><button class="btn programs-btn btn-lg";>Start Now >></button></a></p>
    </section>
    <hr class="hr-zegac">
    <hr class="hr-zegac2" id="section3">

    <!-- Workouts section-->

    <section>
      <h1 class="text-center section-header">Workouts</h1>
      <div id="workouts" class="carousel slide" data-ride="carousel">
  
              <!-- Indicators -->
              <ul class="carousel-indicators">
                <li data-target="#workouts" data-slide-to="0" class="active"></li>
                <li data-target="#workouts" data-slide-to="1"></li>
                <li data-target="#workouts" data-slide-to="2"></li>
              </ul>
            
              <!-- The slideshow -->
              <div class="carousel-inner text-center">
                <div class="carousel-item active">
                  <img src="resource/easy.jpg" alt="Easy">
                  <div class="overlay2">
                  <div class="carousel-caption"></div> 
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="resource/medium.jpg" alt="Medium">
                  <div class="overlay2">
                  <div class="carousel-caption"></div> 
                  </div>
                </div>
                <div class="carousel-item">
                  <img src="resource/hard.jpg" alt="Hard"> 
                  <div class="overlay2">
                  <div class="carousel-caption"></div>  
                  </div>
                </div>
              </div>
              <!-- Left and right controls -->
              <a class="carousel-control-prev" href="#workouts" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#workouts" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>
            <p class="text-center"><a href="userworkouts.php" target="_blank"><button class="btn workouts-btn btn-lg">Try it now >></button></a></p>
      </section>
      <hr class="hr-zegac">
      <hr class="hr-zegac2" id="section4">

    <!-- Contact us section-->
    <div id="contact-us">
    <h1 class="text-center section-header">Contact us</h1>
    <form id="contactform" name="contactform" action="contact.php" onsubmit="return contact_validation()" method="post">
      <div class="contact-form">
        <div class="tekst">
          <label>Full Name :</label>
          <input type="text" name="fullname" id="name" placeholder="Enter your name">
        </div>
        <div class="tekst">
          <label>Email :</label>
          <input type="email" name="email" id="email" placeholder="Enter your email">
        </div>
        <div class="tekst">
          <label>Phone number :</label>
          <input type="text" name="phone_number" id="phone_number" placeholder="Enter your phone">
        </div>
        <div class="tekst">
          <label>Message :</label>
          <textarea id="message" name="message" placeholder="Enter your message for us"></textarea>
          <input type="submit" name="send_message" value="Send Message" class="btn-send">
        </div>
      </div>
    </form>
    </div>

      <hr class="hr-zegac">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script> 
    $(document).ready(function(){
    $("#button-comment").click(function(){
    $("#commentsection").slideToggle("slow");
  });
});
<?php
include ("template/footer.php");
?>
