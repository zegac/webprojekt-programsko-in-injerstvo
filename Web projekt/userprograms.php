<?php
include ("server.php");
if(empty($_SESSION['username'])){
  header('location: index.php');
}
?>

<?php
include ("template/header.php");
?>

<div id="userprograms">
<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4">Pricing</h1>
  <p class="lead">Quickly build an effective pricing table for your potential customers with this Bootstrap example. It’s built with default Bootstrap components and utilities with little customization.</p>
</div>


<div class="container-fluid">
  <div class="card-deck mb-3 text-center">
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal">Beginner</h4>
      </div>
      <div class="card-body">
        <h1 class="card-title pricing-card-title">$10 <small class="text-muted">/ month</small></h1>
        <ul class="list-unstyled mt-3 mb-4">
          <li>10 workouts for full body</li>
          <li>10 Cardio workouts</li>
          <li>2 workouts for every muscle</li>
          <li>Email support</li>
        </ul>
        <form action="buy.php" method="POST">
        <button value="buy1" id="buybutton1" name="buybutton1" type="submit" class="btn btn-lg btn-block btn-outline-primary buy-click1">Buy beginner program</button>
        </form>
    </div>
    </div>
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal">Intermidiate</h4>
      </div>
      <div class="card-body">
        <h1 class="card-title pricing-card-title">$20 <small class="text-muted ">/ month</small></h1>
        <ul class="list-unstyled mt-3 mb-4">
          <li>20 workouts for full body</li>
          <li>20 cardio workouts</li>
          <li>5 workouts for every muscle</li>
          <li>Email support</li>
        </ul>
        <form action="buy.php" method="POST">
        <button value="buy2" id="buybutton2" name="buybutton2" type="submit" class="btn btn-lg btn-block btn-outline-primary buy-click2">Buy intermidiate program</button>
        </form>
    </div>
    </div>
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal">Advanced</h4>
      </div>
      <div class="card-body">
        <h1 class="card-title pricing-card-title">$30 <small class="text-muted">/ month</small></h1>
        <ul class="list-unstyled mt-3 mb-4">
          <li>50+ workouts for full body</li>
          <li>50+ cardio workouts</li>
          <li>50+ workouts for every muscle</li>
          <li>Nutrition tips and tricks</li>
        </ul>
        <form action="buy.php" method="POST">
        <button value="buy3" id="buybutton3" name="buybutton3" type="submit" class="btn btn-lg btn-block btn-outline-primary buy-click3">Buy advanced program</button>
        </form>
    </div>
    </div>
  </div>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="skripta3.js"></script>
<?php
include ("template/footer.php");
?>
